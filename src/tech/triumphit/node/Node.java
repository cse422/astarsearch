package tech.triumphit.node;

public class Node {
    public  String value;
    public double g_scores;
    public  double h_scores;
    public double f_scores = 0;
    public Edge[] adjacencies;
    public Node parent;

    public Node(String val, double hVal){
        value = val;
        h_scores = hVal;
    }

    @Override
    public String toString(){
        return value;
    }
}
